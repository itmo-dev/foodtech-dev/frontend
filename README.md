# frontend

## Установка проекта
```
npm install
```

### Компилирует и перезагружает проект при изменениях
```
npm run serve
```

### Компилирует и сжимает для продакшена
```
npm run build
```

### Тесты
```
npm run test
```

### Линтинг
```
npm run lint
```

### Изменение кофигурации
Сморти [Configuration Reference](https://cli.vuejs.org/config/).
